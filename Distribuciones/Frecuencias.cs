﻿using MoreLinq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Distribuciones
{
    public class Interval
    {
        public float lower_bound { get; set; }
        public float upper_bound { get; set; }
        public float expected_freq { get; set; }
        public float observed_freq { get; set; }
        public float chi_squared { get; set; }
        public Interval()
        {
            this.observed_freq = 0;
        }

        public override string ToString()
        {
            return String.Format("\n{0}-{1}: \n EF-> {2} \n OF-> {3} \n Chi-Sq-> {4}",
                this.lower_bound.ToString("0.0000"), this.upper_bound.ToString("0.0000"),
                this.expected_freq, this.observed_freq, this.chi_squared.ToString("0.0000"));
        }

        public string GetIntervalBounds()
        {
            return String.Format("{0}-{1}", this.lower_bound.ToString("0.0000"),
                this.upper_bound.ToString("0.0000"));
        }

        public static float GetPaso(float cantidad_intervalos, IEnumerable<Number> valores)
        {
            return ((float)valores.OrderByDescending(x => x.random).First().random - (float)valores.OrderByDescending(x => x.random).Reverse().First().random) / cantidad_intervalos;
        }
    }

    public class Frecuencias
    {
        public static bool ValidateDataInput(Dictionary<string, string> keyValuePairs)
        {
            float result;
            int resultIntervals;
            bool valid = true;
            foreach (KeyValuePair<string, string> kvp in keyValuePairs)
            {
                if (float.TryParse(kvp.Value, out result) && (result >= 0))
                {
                    Console.WriteLine("Key: {0}, Value: {1}", kvp.Key, kvp.Value);
                }
                else
                {
                    valid = false;
                    MessageBox.Show(String.Format("Ingrese un valor numerico positivo para: {0}", kvp.Key));
                    break;
                }
            }
            if (float.TryParse(keyValuePairs["Cantidad intervalos"], out result) && result == 0)
            {
                valid = false;
                MessageBox.Show("Ingrese una cantidad de intervalos > a 0.");
            }
            else if (!(int.TryParse(keyValuePairs["Cantidad intervalos"], out resultIntervals)) ||
                !(int.TryParse(keyValuePairs["Cantidad números"], out resultIntervals)))
            {
                valid = false;
                MessageBox.Show("Ingrese un numero entero positivo para cantida de intervalos y numeros.");
            }
            return valid;
        }

        private void SetFrequenciesAndExport(IEnumerable<double> list_randoms, List<Interval> intervals)
        {
            TextWriter tw = new StreamWriter("list.txt");
            var counter = 1;
            foreach (float num in list_randoms)
            {
                //Export numbers to .txt file
                tw.Write(num.ToString("0.0000") + ", ");
                if (counter % 11 == 0)
                {
                    tw.Write("\n");
                }
                counter++;

                //Find the interval
                foreach (Interval i in intervals)
                {
                    if (num <= i.upper_bound && num >= i.lower_bound)
                    {
                        i.observed_freq++;
                        break;
                    }
                }
            }

            //Set chi-squared for each interval
            foreach (Interval i in intervals)
            {
                i.chi_squared = (float)(Math.Pow((double)(i.observed_freq - i.expected_freq), 2) / i.expected_freq);
            }
            tw.Close();
        }

        public static List<Interval> SetFrequencies(IEnumerable<Number> list_randoms, List<Interval> intervals)
        {

            foreach (Number num in list_randoms)
            {
                //Find the interval
                foreach (Interval i in intervals)
                {
                    if (num.random <= i.upper_bound && num.random >= i.lower_bound)
                    {
                        i.observed_freq++;
                        break;
                    }
                }
            }

            //Set chi-squared for each interval
            foreach (Interval i in intervals)
            {
                i.chi_squared = (float)(Math.Pow((double)(i.observed_freq - i.expected_freq), 2) / i.expected_freq);
                //Console.WriteLine("{0} -> EF: {1} | OF: {2} | CHI: {3}", i.GetIntervalBounds(), i.expected_freq, i.observed_freq, i.chi_squared);
            }
            return intervals;
        }

        public static List<Interval> SetIntervals(float max, float min, int int_q, float paso)
        {
            List<Interval> intervals = new List<Interval>();
            Interval aux = new Interval();

            //Set first interval
            aux.lower_bound = (float)min;
            aux.upper_bound = (float)min + paso;
            intervals.Add(aux);

            //Set the following intervals from 1 to intervals quantity
            for (int i = 1; i < int_q; i++)
            {
                aux = new Interval();
                aux.lower_bound = (float)intervals[intervals.Count - 1].upper_bound;
                aux.upper_bound = (float)aux.lower_bound + paso;
                intervals.Add(aux);
            }

            return intervals;
        }

    }
}

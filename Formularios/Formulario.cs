﻿using Distribuciones;
using MoreLinq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MathNet.Numerics.Distributions;

namespace Formularios
{
    public partial class Formulario : Form
    {
        private List<Interval> intervalos;
        private Dictionary<string, string> parametros = new Dictionary<string, string>();
        private Pagina pagina;

        public int grados_libertad = 0;
        public float nivel_confianza = 0.01f;
        private bool generado = false;

        public Formulario()
        {
            InitializeComponent();
        }

        private void Formulario_Load(object sender, EventArgs e)
        {

        }

        private void BtnVerificar_Click(object sender, EventArgs e)
        {
            Probrar_Chi();
        }

        private void BtnGenerar_Click(object sender, EventArgs e)
        {
            switch (cmbMetodo.Text)
            {
                case "Uniforme":
                    Mtd_Uniforme();
                    break;
                case "Poisson":
                    Mtd_Poisson();
                    break;
                case "Exponencial Negativa":
                    Mtd_ExpNeg();
                    break;
                case "Normal":
                    Mtd_Normal();
                    break;
                default:
                    MessageBox.Show("Seleccione un método antes de generar la distribución.");
                    break;
            }
        }

        private void Probrar_Chi()
        {
            if (this.generado)
            {
                if (grados_libertad > 0)
                {
                    float result;
                    float suma_chi = float.Parse(txtSumatoriaChi.Text);
                    if (float.TryParse(txtNivelConfianza.Text, out result) && result > 0 && result < 1)
                    {
                        float valorTabla = (float)ChiSquared.InvCDF((double)grados_libertad, 1 - (double)result);
                        txtValorTabla.Text = valorTabla.ToString("0.0000");

                        if (valorTabla > suma_chi)
                        {
                            txtResultadoChi.Text = "Se aprueba";
                        }
                        else
                        {
                            txtResultadoChi.Text = "Se rechaza";

                        }
                    }
                    else
                    {
                        MessageBox.Show("Ingrese un valor entre cero y uno para Nivel Confianza");
                    }
                }
                else
                {
                    MessageBox.Show("Grados de libertad menor o igual a cero.");
                }
            }
            else
            {
                MessageBox.Show("Genere una distribucion para calcular Chi.");
            }
        }

        private void Mtd_Uniforme()
        {

            if (intervalos != null)
            {
                intervalos = new List<Interval>();
            }

            parametros.Clear();
            parametros.Add(txtCotaInferior.Tag.ToString(), txtCotaInferior.Text);
            parametros.Add(txtCotaSuperior.Tag.ToString(), txtCotaSuperior.Text);
            parametros.Add(txtCantidadNumeros.Tag.ToString(), txtCantidadNumeros.Text);
            parametros.Add(txtCantidadIntervalos.Tag.ToString(), txtCantidadIntervalos.Text);

            if (Frecuencias.ValidateDataInput(parametros))
            {
                float rango_inf;
                float rango_sup;

                int cantidad_intervalos = int.Parse(txtCantidadIntervalos.Text);

                float a = float.Parse(txtCotaInferior.Text);
                float b = float.Parse(txtCotaSuperior.Text);

                Uniforme u = new Uniforme(a, b);

                List<Number> variables = u.ObtenerGeneradorUniforme(long.Parse(txtCantidadNumeros.Text)).ToList();

                if (float.TryParse(txtRangoInf.Text, out rango_inf) && float.TryParse(txtRangoSup.Text, out rango_sup)
                    && rango_inf < rango_sup)
                {

                    List<Number> mostrar = variables.Where(x => x.index >= rango_inf
                                                            && x.index <= rango_sup).ToList();
                    if (mostrar.Count() != 0)
                    {
                        pagina = new Pagina(mostrar);
                    }
                    else
                    {
                        pagina = new Pagina(variables);

                    }
                }
                else
                {
                    pagina = new Pagina(variables);

                }

                Llenar_DGV_Valores(pagina.Get_Actual());

                float paso = Interval.GetPaso(cantidad_intervalos, variables);

                float max = variables.MaxBy(x => x.random).First().random;
                float min = variables.MinBy(x => x.random).First().random;

                intervalos = Frecuencias.SetIntervals(max, min, cantidad_intervalos, paso);
                foreach (Interval intervalo in intervalos)
                {
                    intervalo.expected_freq = float.Parse(txtCantidadNumeros.Text) / (float)cantidad_intervalos;
                }

                intervalos = Frecuencias.SetFrequencies(variables, intervalos);

                Llenar_DGV_Frecuencias(intervalos);

                grados_libertad = intervalos.Count() - 2 - 1;

                generado = true;
            }

        }

        private void Mtd_Normal()
        {

            if (intervalos != null)
            {
                intervalos = new List<Interval>();
            }

            parametros.Clear();
            parametros.Add(txtMedia.Tag.ToString(), txtMedia.Text);
            parametros.Add(txtDesviacion.Tag.ToString(), txtDesviacion.Text);
            parametros.Add(txtCantidadNumeros.Tag.ToString(), txtCantidadNumeros.Text);
            parametros.Add(txtCantidadIntervalos.Tag.ToString(), txtCantidadIntervalos.Text);

            if (Frecuencias.ValidateDataInput(parametros))
            {
                float rango_inf;
                float rango_sup;

                int cantidad_intervalos = int.Parse(txtCantidadIntervalos.Text);

                float media = float.Parse(txtMedia.Text);
                float desviacion = float.Parse(txtDesviacion.Text);

                Distribuciones.Normal n = new Distribuciones.Normal(media, desviacion);

                List<Number> variables = n.ObtenerGeneradorNormal(long.Parse(txtCantidadNumeros.Text)).ToList();

                if (float.TryParse(txtRangoInf.Text, out rango_inf) && float.TryParse(txtRangoSup.Text, out rango_sup)
                    && rango_inf < rango_sup)
                {

                    List<Number> mostrar = variables.Where(x => x.index >= rango_inf
                                                            && x.index <= rango_sup).ToList();
                    if (mostrar.Count() != 0)
                    {
                        pagina = new Pagina(mostrar);
                    }
                    else
                    {
                        pagina = new Pagina(variables);

                    }
                }
                else
                {
                    pagina = new Pagina(variables);

                }

                Llenar_DGV_Valores(pagina.Get_Actual());

                float paso = Interval.GetPaso(cantidad_intervalos, variables);

                float max = variables.OrderByDescending(x => x.random).FirstOrDefault().random;
                float min = variables.OrderByDescending(x => x.random).Reverse().First().random;

                intervalos = Frecuencias.SetIntervals(max, min, cantidad_intervalos, paso);


                foreach (Interval intervalo in intervalos)
                {
                    float upper_norm = (float)MathNet.Numerics.Distributions.Normal.CDF((double)media, (double)desviacion, (double)intervalo.upper_bound);
                    float lower_norm = (float)MathNet.Numerics.Distributions.Normal.CDF((double)media, (double)desviacion, (double)intervalo.lower_bound);

                    intervalo.expected_freq = (upper_norm - lower_norm) * float.Parse(txtCantidadNumeros.Text);
                }

                intervalos = Frecuencias.SetFrequencies(variables, intervalos);

                Llenar_DGV_Frecuencias(intervalos);

                grados_libertad = intervalos.Count() - 2 - 1;

                generado = true;
            }

        }

        private void Mtd_ExpNeg()
        {

            if (intervalos != null)
            {
                intervalos = new List<Interval>();
            }

            parametros.Clear();
            parametros.Add(txtMedia.Tag.ToString(), txtMedia.Text);
            parametros.Add(txtCantidadNumeros.Tag.ToString(), txtCantidadNumeros.Text);
            parametros.Add(txtCantidadIntervalos.Tag.ToString(), txtCantidadIntervalos.Text);

            if (Frecuencias.ValidateDataInput(parametros))
            {
                float rango_inf;
                float rango_sup;

                int cantidad_intervalos = int.Parse(txtCantidadIntervalos.Text);

                float media = float.Parse(txtMedia.Text);
                float lambda = float.Parse(txtLambda.Text);



                if (lambda >= -1)
                {

                    Distribuciones.ExponencialNegativa en = new Distribuciones.ExponencialNegativa(lambda, media);

                    if (lambda == -1)
                    {
                        lambda = 1 / media;
                    }

                    List<Number> variables = en.ObtenerGeneradorExponencialNegativa(long.Parse(txtCantidadNumeros.Text)).ToList();

                    if (float.TryParse(txtRangoInf.Text, out rango_inf) && float.TryParse(txtRangoSup.Text, out rango_sup)
                        && rango_inf < rango_sup)
                    {

                        List<Number> mostrar = variables.Where(x => x.index >= rango_inf
                                                                && x.index <= rango_sup).ToList();
                        if (mostrar.Count() != 0)
                        {
                            pagina = new Pagina(mostrar);
                        }
                        else
                        {
                            pagina = new Pagina(variables);

                        }
                    }
                    else
                    {
                        pagina = new Pagina(variables);

                    }

                    Llenar_DGV_Valores(pagina.Get_Actual());

                    float paso = Interval.GetPaso(cantidad_intervalos, variables);

                    float max = variables.OrderByDescending(x => x.random).FirstOrDefault().random;
                    float min = variables.OrderByDescending(x => x.random).Reverse().First().random;

                    intervalos = Frecuencias.SetIntervals(max, min, cantidad_intervalos, paso);


                    foreach (Interval intervalo in intervalos)
                    {
                        float upper_norm = (float)MathNet.Numerics.Distributions.Exponential.CDF((double)lambda, (double)intervalo.upper_bound);
                        float lower_norm = (float)MathNet.Numerics.Distributions.Exponential.CDF((double)lambda, (double)intervalo.lower_bound);

                        intervalo.expected_freq = (upper_norm - lower_norm) * float.Parse(txtCantidadNumeros.Text);
                    }

                    intervalos = Frecuencias.SetFrequencies(variables, intervalos);

                    Llenar_DGV_Frecuencias(intervalos);

                    grados_libertad = intervalos.Count() - 1 - 1;

                    generado = true;
                }
                else
                {
                    MessageBox.Show("Ingrese un valor de lambda >= -1. En caso de ser -1 se tomará lambda = 1/media");
                }


            }

        }

        private void Mtd_Poisson()
        {

            if (intervalos != null)
            {
                intervalos = new List<Interval>();
            }

            parametros.Clear();
            parametros.Add(txtLambda.Tag.ToString(), txtLambda.Text);
            parametros.Add(txtCantidadNumeros.Tag.ToString(), txtCantidadNumeros.Text);
            parametros.Add(txtCantidadIntervalos.Tag.ToString(), txtCantidadIntervalos.Text);

            if (Frecuencias.ValidateDataInput(parametros))
            {
                float rango_inf;
                float rango_sup;

                int cantidad_intervalos = int.Parse(txtCantidadIntervalos.Text);

                float lambda = float.Parse(txtLambda.Text);

                Distribuciones.Poisson en = new Distribuciones.Poisson(lambda);

                List<Number> variables = en.ObtenerGeneradorPoisson(long.Parse(txtCantidadNumeros.Text)).ToList();

                if (float.TryParse(txtRangoInf.Text, out rango_inf) && float.TryParse(txtRangoSup.Text, out rango_sup)
                    && rango_inf < rango_sup)
                {

                    List<Number> mostrar = variables.Where(x => x.index >= rango_inf
                                                            && x.index <= rango_sup).ToList();
                    if (mostrar.Count() != 0)
                    {
                        pagina = new Pagina(mostrar);
                    }
                    else
                    {
                        pagina = new Pagina(variables);

                    }
                }
                else
                {
                    pagina = new Pagina(variables);

                }

                Llenar_DGV_Valores(pagina.Get_Actual());

                float paso = Interval.GetPaso(cantidad_intervalos, variables);

                float max = variables.OrderByDescending(x => x.random).FirstOrDefault().random;
                float min = variables.OrderByDescending(x => x.random).Reverse().First().random;

                intervalos = Frecuencias.SetIntervals(max, min, cantidad_intervalos, paso);


                foreach (Interval intervalo in intervalos)
                {
                    float upper_norm = (float)MathNet.Numerics.Distributions.Poisson.CDF((double)lambda, (double)intervalo.upper_bound);
                    float lower_norm = (float)MathNet.Numerics.Distributions.Poisson.CDF((double)lambda, (double)intervalo.lower_bound);

                    intervalo.expected_freq = (upper_norm - lower_norm) * float.Parse(txtCantidadNumeros.Text);
                }

                intervalos = Frecuencias.SetFrequencies(variables, intervalos);

                Llenar_DGV_Frecuencias(intervalos);

                grados_libertad = intervalos.Count() - 1 - 1;

                generado = true;
            }

        }

        private void Mtd_Uniforme_Habilitar()
        {

            txtCotaInferior.Enabled = true;
            txtCotaSuperior.Enabled = true;

            txtDesviacion.Enabled = false;
            txtLambda.Enabled = false;
            txtMedia.Enabled = false;

        }

        private void Llenar_DGV_Valores(IEnumerable<Number> valores)
        {
            dgvValores.Rows.Clear();

            foreach (Number valor in valores)
            {
                dgvValores.Rows.Add(valor.index.ToString(), valor.random.ToString());
            }

        }

        private void Llenar_DGV_Frecuencias(List<Interval> intervals)
        {
            dgvFrecuencias.Rows.Clear();

            chtFrecuencias.Series["FE"].Points.Clear();
            chtFrecuencias.Series["FO"].Points.Clear();

            float chi_sum = 0;

            foreach (Interval i in intervals)
            {
                chi_sum += (float)i.chi_squared;

                dgvFrecuencias.Rows.Add(i.GetIntervalBounds(),
                    i.expected_freq.ToString("0.0"), i.observed_freq.ToString("0.0"),
                    i.chi_squared.ToString("0.0000"));
                chtFrecuencias.Series["FE"].Points.AddXY(i.GetIntervalBounds(), i.expected_freq);
                chtFrecuencias.Series["FO"].Points.AddXY(i.GetIntervalBounds(), i.observed_freq);
            }

            txtSumatoriaChi.Text = chi_sum.ToString("0.000");
        }

        private void BtnAnterior_Click(object sender, EventArgs e)
        {
            if (generado)
            {
                Llenar_DGV_Valores(pagina.Get_Anterior());
            }
        }

        private void BtnSiguiente_Click(object sender, EventArgs e)
        {
            if (generado)
            {
                Llenar_DGV_Valores(pagina.Get_Siguiente());
            }
        }

        private void CmbMetodo_SelectedValueChanged(object sender, EventArgs e)
        {
            Limpiar_Campos();
            Limpiar_Chart();
            Limpiar_DGV();
            switch (cmbMetodo.Text)
            {
                case "Uniforme":
                    Mtd_Uniforme_Habilitar();
                    break;
                case "Poisson":
                    Mtd_Poisson_Habilitar();
                    break;
                case "Exponencial Negativa":
                    Mtd_ExpNeg_Habilitar();
                    break;
                case "Normal":
                    Mtd_Normal_Habilitar();
                    break;
                default:
                    break;
            }
        }

        private void Mtd_Poisson_Habilitar()
        {
            txtLambda.Enabled = true;

            txtDesviacion.Enabled = false;
            txtMedia.Enabled = false;
            txtCotaInferior.Enabled = false;
            txtCotaSuperior.Enabled = false;
        }

        private void Mtd_ExpNeg_Habilitar()
        {
            txtMedia.Enabled = true;
            txtLambda.Enabled = true;

            txtCotaInferior.Enabled = false;
            txtCotaSuperior.Enabled = false;
            txtDesviacion.Enabled = false;
        }

        private void Mtd_Normal_Habilitar()
        {
            txtMedia.Enabled = true;
            txtDesviacion.Enabled = true;

            txtCotaInferior.Enabled = false;
            txtCotaSuperior.Enabled = false;
            txtLambda.Enabled = false;
        }

        private void Limpiar_DGV()
        {
            dgvFrecuencias.Rows.Clear();
            dgvValores.Rows.Clear();
        }

        private void Limpiar_Campos()
        {
            txtDesviacion.Text = "";
            txtMedia.Text = "";
            txtLambda.Text = "";
            txtCotaInferior.Text = "";
            txtCotaSuperior.Text = "";
            txtRangoInf.Text = "";
            txtRangoSup.Text = "";
            txtSumatoriaChi.Text = "";

            txtResultadoChi.Text = "";
            txtValorTabla.Text = "";
            grados_libertad = 0;
        }

        private void Limpiar_Chart()
        {
            chtFrecuencias.Series["FE"].Points.Clear();
            chtFrecuencias.Series["FO"].Points.Clear();
        }

    }

    class Pagina
    {
        public long items_por_pagina { get; set; }
        public long limite_inferior { get; set; }
        public long limite_superior { get; set; }
        public List<Number> valores { get; set; }

        public Pagina(List<Number> valores)
        {
            this.limite_inferior = (long)valores[0].index;
            this.limite_superior = (long)valores[0].index + 1000;
            this.valores = valores;
            this.items_por_pagina = 1000;
        }
        public IEnumerable<Number> Get_Siguiente()
        {
            if (!(this.limite_inferior + this.items_por_pagina > (long)valores[valores.Count() - 1].index))
            {
                this.limite_inferior += this.items_por_pagina;
                this.limite_superior += this.items_por_pagina;
            }

            return valores.Where(x => x.index >= this.limite_inferior && x.index <= this.limite_superior);
        }
        public IEnumerable<Number> Get_Anterior()
        {
            if (!(this.limite_inferior - this.items_por_pagina < (long)valores[0].index))
            {
                this.limite_inferior -= this.items_por_pagina;
                this.limite_superior -= this.items_por_pagina;
            }

            return valores.Where(x => x.index >= this.limite_inferior && x.index <= this.limite_superior);
        }

        public IEnumerable<Number> Get_Actual()
        {
            return valores.Where(x => x.index >= this.limite_inferior && x.index <= this.limite_superior);
        }
    }
}



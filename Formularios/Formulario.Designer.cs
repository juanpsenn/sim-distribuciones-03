﻿namespace Formularios
{
    partial class Formulario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.Title title3 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Formulario));
            this.cmbMetodo = new System.Windows.Forms.ComboBox();
            this.lblMetodo = new System.Windows.Forms.Label();
            this.txtCotaInferior = new System.Windows.Forms.TextBox();
            this.txtCotaSuperior = new System.Windows.Forms.TextBox();
            this.lblCotaInferior = new System.Windows.Forms.Label();
            this.lblCotaSuperior = new System.Windows.Forms.Label();
            this.lblMedia = new System.Windows.Forms.Label();
            this.txtMedia = new System.Windows.Forms.TextBox();
            this.lblDesviacion = new System.Windows.Forms.Label();
            this.txtDesviacion = new System.Windows.Forms.TextBox();
            this.lblLambda = new System.Windows.Forms.Label();
            this.txtLambda = new System.Windows.Forms.TextBox();
            this.lblCantidadNumeros = new System.Windows.Forms.Label();
            this.txtCantidadNumeros = new System.Windows.Forms.TextBox();
            this.lblCantidadIntervalos = new System.Windows.Forms.Label();
            this.txtCantidadIntervalos = new System.Windows.Forms.TextBox();
            this.btnGenerar = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.lblGrillaValores = new System.Windows.Forms.Label();
            this.lblRangoInferior = new System.Windows.Forms.Label();
            this.txtRangoInf = new System.Windows.Forms.TextBox();
            this.lblRangoSup = new System.Windows.Forms.Label();
            this.txtRangoSup = new System.Windows.Forms.TextBox();
            this.dgvValores = new System.Windows.Forms.DataGridView();
            this.index = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvFrecuencias = new System.Windows.Forms.DataGridView();
            this.intervalo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chi_sqr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblDistribucionFrecuencias = new System.Windows.Forms.Label();
            this.lblSumatoria = new System.Windows.Forms.Label();
            this.txtSumatoriaChi = new System.Windows.Forms.TextBox();
            this.btnSiguiente = new System.Windows.Forms.Button();
            this.btnAnterior = new System.Windows.Forms.Button();
            this.lblPaginas = new System.Windows.Forms.Label();
            this.chtFrecuencias = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.txtValorTabla = new System.Windows.Forms.TextBox();
            this.lvlValorTabla = new System.Windows.Forms.Label();
            this.txtNivelConfianza = new System.Windows.Forms.TextBox();
            this.lblNivelConfianza = new System.Windows.Forms.Label();
            this.lblAprobacion = new System.Windows.Forms.Label();
            this.txtResultadoChi = new System.Windows.Forms.TextBox();
            this.btnVerificarChi = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvValores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFrecuencias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chtFrecuencias)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbMetodo
            // 
            this.cmbMetodo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMetodo.FormattingEnabled = true;
            this.cmbMetodo.Items.AddRange(new object[] {
            "Uniforme",
            "Exponencial Negativa",
            "Normal",
            "Poisson"});
            this.cmbMetodo.Location = new System.Drawing.Point(12, 24);
            this.cmbMetodo.Name = "cmbMetodo";
            this.cmbMetodo.Size = new System.Drawing.Size(216, 24);
            this.cmbMetodo.TabIndex = 1;
            this.cmbMetodo.SelectedValueChanged += new System.EventHandler(this.CmbMetodo_SelectedValueChanged);
            // 
            // lblMetodo
            // 
            this.lblMetodo.AutoSize = true;
            this.lblMetodo.Location = new System.Drawing.Point(12, 4);
            this.lblMetodo.Name = "lblMetodo";
            this.lblMetodo.Size = new System.Drawing.Size(55, 17);
            this.lblMetodo.TabIndex = 1;
            this.lblMetodo.Text = "Método";
            // 
            // txtCotaInferior
            // 
            this.txtCotaInferior.Enabled = false;
            this.txtCotaInferior.Location = new System.Drawing.Point(12, 80);
            this.txtCotaInferior.MaxLength = 8;
            this.txtCotaInferior.Name = "txtCotaInferior";
            this.txtCotaInferior.Size = new System.Drawing.Size(216, 22);
            this.txtCotaInferior.TabIndex = 2;
            this.txtCotaInferior.Tag = "Cota inferior (a)";
            // 
            // txtCotaSuperior
            // 
            this.txtCotaSuperior.Enabled = false;
            this.txtCotaSuperior.Location = new System.Drawing.Point(12, 125);
            this.txtCotaSuperior.MaxLength = 8;
            this.txtCotaSuperior.Name = "txtCotaSuperior";
            this.txtCotaSuperior.Size = new System.Drawing.Size(216, 22);
            this.txtCotaSuperior.TabIndex = 3;
            this.txtCotaSuperior.Tag = "Cota superior (b)";
            // 
            // lblCotaInferior
            // 
            this.lblCotaInferior.AutoSize = true;
            this.lblCotaInferior.Location = new System.Drawing.Point(12, 60);
            this.lblCotaInferior.Name = "lblCotaInferior";
            this.lblCotaInferior.Size = new System.Drawing.Size(107, 17);
            this.lblCotaInferior.TabIndex = 4;
            this.lblCotaInferior.Text = "Cota inferior (a)";
            // 
            // lblCotaSuperior
            // 
            this.lblCotaSuperior.AutoSize = true;
            this.lblCotaSuperior.Location = new System.Drawing.Point(12, 105);
            this.lblCotaSuperior.Name = "lblCotaSuperior";
            this.lblCotaSuperior.Size = new System.Drawing.Size(115, 17);
            this.lblCotaSuperior.TabIndex = 5;
            this.lblCotaSuperior.Text = "Cota superior (b)";
            // 
            // lblMedia
            // 
            this.lblMedia.AutoSize = true;
            this.lblMedia.Location = new System.Drawing.Point(12, 150);
            this.lblMedia.Name = "lblMedia";
            this.lblMedia.Size = new System.Drawing.Size(46, 17);
            this.lblMedia.TabIndex = 7;
            this.lblMedia.Text = "Media";
            // 
            // txtMedia
            // 
            this.txtMedia.Enabled = false;
            this.txtMedia.Location = new System.Drawing.Point(12, 170);
            this.txtMedia.MaxLength = 8;
            this.txtMedia.Name = "txtMedia";
            this.txtMedia.Size = new System.Drawing.Size(216, 22);
            this.txtMedia.TabIndex = 4;
            this.txtMedia.Tag = "Media";
            // 
            // lblDesviacion
            // 
            this.lblDesviacion.AutoSize = true;
            this.lblDesviacion.Location = new System.Drawing.Point(12, 195);
            this.lblDesviacion.Name = "lblDesviacion";
            this.lblDesviacion.Size = new System.Drawing.Size(77, 17);
            this.lblDesviacion.TabIndex = 9;
            this.lblDesviacion.Text = "Desviacion";
            // 
            // txtDesviacion
            // 
            this.txtDesviacion.Enabled = false;
            this.txtDesviacion.Location = new System.Drawing.Point(12, 215);
            this.txtDesviacion.MaxLength = 8;
            this.txtDesviacion.Name = "txtDesviacion";
            this.txtDesviacion.Size = new System.Drawing.Size(216, 22);
            this.txtDesviacion.TabIndex = 5;
            this.txtDesviacion.Tag = "Desviación";
            // 
            // lblLambda
            // 
            this.lblLambda.AutoSize = true;
            this.lblLambda.Location = new System.Drawing.Point(12, 240);
            this.lblLambda.Name = "lblLambda";
            this.lblLambda.Size = new System.Drawing.Size(59, 17);
            this.lblLambda.TabIndex = 11;
            this.lblLambda.Text = "Lambda";
            // 
            // txtLambda
            // 
            this.txtLambda.Enabled = false;
            this.txtLambda.Location = new System.Drawing.Point(12, 260);
            this.txtLambda.MaxLength = 8;
            this.txtLambda.Name = "txtLambda";
            this.txtLambda.Size = new System.Drawing.Size(216, 22);
            this.txtLambda.TabIndex = 6;
            this.txtLambda.Tag = "Lambda";
            // 
            // lblCantidadNumeros
            // 
            this.lblCantidadNumeros.AutoSize = true;
            this.lblCantidadNumeros.Location = new System.Drawing.Point(249, 4);
            this.lblCantidadNumeros.Name = "lblCantidadNumeros";
            this.lblCantidadNumeros.Size = new System.Drawing.Size(123, 17);
            this.lblCantidadNumeros.TabIndex = 13;
            this.lblCantidadNumeros.Text = "Cantidad números";
            // 
            // txtCantidadNumeros
            // 
            this.txtCantidadNumeros.Location = new System.Drawing.Point(249, 24);
            this.txtCantidadNumeros.MaxLength = 8;
            this.txtCantidadNumeros.Name = "txtCantidadNumeros";
            this.txtCantidadNumeros.Size = new System.Drawing.Size(216, 22);
            this.txtCantidadNumeros.TabIndex = 7;
            this.txtCantidadNumeros.Tag = "Cantidad números";
            // 
            // lblCantidadIntervalos
            // 
            this.lblCantidadIntervalos.AutoSize = true;
            this.lblCantidadIntervalos.Location = new System.Drawing.Point(479, 4);
            this.lblCantidadIntervalos.Name = "lblCantidadIntervalos";
            this.lblCantidadIntervalos.Size = new System.Drawing.Size(129, 17);
            this.lblCantidadIntervalos.TabIndex = 15;
            this.lblCantidadIntervalos.Text = "Cantidad intervalos";
            // 
            // txtCantidadIntervalos
            // 
            this.txtCantidadIntervalos.Location = new System.Drawing.Point(479, 24);
            this.txtCantidadIntervalos.MaxLength = 2;
            this.txtCantidadIntervalos.Name = "txtCantidadIntervalos";
            this.txtCantidadIntervalos.Size = new System.Drawing.Size(216, 22);
            this.txtCantidadIntervalos.TabIndex = 8;
            this.txtCantidadIntervalos.Tag = "Cantidad intervalos";
            // 
            // btnGenerar
            // 
            this.btnGenerar.Location = new System.Drawing.Point(12, 288);
            this.btnGenerar.Name = "btnGenerar";
            this.btnGenerar.Size = new System.Drawing.Size(216, 23);
            this.btnGenerar.TabIndex = 11;
            this.btnGenerar.Text = "GENERAR DIST.";
            this.btnGenerar.UseVisualStyleBackColor = true;
            this.btnGenerar.Click += new System.EventHandler(this.BtnGenerar_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(12, 317);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(216, 23);
            this.btnLimpiar.TabIndex = 16;
            this.btnLimpiar.Text = "LIMPIAR";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            // 
            // lblGrillaValores
            // 
            this.lblGrillaValores.AutoSize = true;
            this.lblGrillaValores.Location = new System.Drawing.Point(249, 60);
            this.lblGrillaValores.Name = "lblGrillaValores";
            this.lblGrillaValores.Size = new System.Drawing.Size(128, 17);
            this.lblGrillaValores.TabIndex = 17;
            this.lblGrillaValores.Text = "Valores generados";
            // 
            // lblRangoInferior
            // 
            this.lblRangoInferior.AutoSize = true;
            this.lblRangoInferior.Location = new System.Drawing.Point(709, 4);
            this.lblRangoInferior.Name = "lblRangoInferior";
            this.lblRangoInferior.Size = new System.Drawing.Size(60, 17);
            this.lblRangoInferior.TabIndex = 19;
            this.lblRangoInferior.Text = "Cota inf.";
            // 
            // txtRangoInf
            // 
            this.txtRangoInf.Location = new System.Drawing.Point(709, 24);
            this.txtRangoInf.MaxLength = 8;
            this.txtRangoInf.Name = "txtRangoInf";
            this.txtRangoInf.Size = new System.Drawing.Size(85, 22);
            this.txtRangoInf.TabIndex = 9;
            this.txtRangoInf.Tag = "Cota inferior";
            // 
            // lblRangoSup
            // 
            this.lblRangoSup.AutoSize = true;
            this.lblRangoSup.Location = new System.Drawing.Point(810, 4);
            this.lblRangoSup.Name = "lblRangoSup";
            this.lblRangoSup.Size = new System.Drawing.Size(68, 17);
            this.lblRangoSup.TabIndex = 21;
            this.lblRangoSup.Text = "Cota sup.";
            // 
            // txtRangoSup
            // 
            this.txtRangoSup.Location = new System.Drawing.Point(810, 24);
            this.txtRangoSup.MaxLength = 8;
            this.txtRangoSup.Name = "txtRangoSup";
            this.txtRangoSup.Size = new System.Drawing.Size(85, 22);
            this.txtRangoSup.TabIndex = 10;
            this.txtRangoSup.Tag = "Cota superior";
            // 
            // dgvValores
            // 
            this.dgvValores.AllowUserToAddRows = false;
            this.dgvValores.AllowUserToDeleteRows = false;
            this.dgvValores.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvValores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvValores.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.index,
            this.valor});
            this.dgvValores.Location = new System.Drawing.Point(249, 80);
            this.dgvValores.Name = "dgvValores";
            this.dgvValores.ReadOnly = true;
            this.dgvValores.RowHeadersVisible = false;
            this.dgvValores.RowHeadersWidth = 51;
            this.dgvValores.RowTemplate.Height = 24;
            this.dgvValores.Size = new System.Drawing.Size(216, 202);
            this.dgvValores.TabIndex = 22;
            // 
            // index
            // 
            this.index.HeaderText = "#";
            this.index.MinimumWidth = 6;
            this.index.Name = "index";
            this.index.ReadOnly = true;
            this.index.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // valor
            // 
            this.valor.HeaderText = "Valor";
            this.valor.MinimumWidth = 6;
            this.valor.Name = "valor";
            this.valor.ReadOnly = true;
            this.valor.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dgvFrecuencias
            // 
            this.dgvFrecuencias.AllowUserToAddRows = false;
            this.dgvFrecuencias.AllowUserToDeleteRows = false;
            this.dgvFrecuencias.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvFrecuencias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFrecuencias.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.intervalo,
            this.fe,
            this.fo,
            this.chi_sqr});
            this.dgvFrecuencias.Location = new System.Drawing.Point(482, 80);
            this.dgvFrecuencias.Name = "dgvFrecuencias";
            this.dgvFrecuencias.ReadOnly = true;
            this.dgvFrecuencias.RowHeadersVisible = false;
            this.dgvFrecuencias.RowHeadersWidth = 51;
            this.dgvFrecuencias.RowTemplate.Height = 24;
            this.dgvFrecuencias.Size = new System.Drawing.Size(581, 202);
            this.dgvFrecuencias.TabIndex = 23;
            // 
            // intervalo
            // 
            this.intervalo.HeaderText = "Intervalo";
            this.intervalo.MinimumWidth = 6;
            this.intervalo.Name = "intervalo";
            this.intervalo.ReadOnly = true;
            // 
            // fe
            // 
            this.fe.HeaderText = "Frec. Esp.";
            this.fe.MinimumWidth = 6;
            this.fe.Name = "fe";
            this.fe.ReadOnly = true;
            // 
            // fo
            // 
            this.fo.HeaderText = "Frec. Obs.";
            this.fo.MinimumWidth = 6;
            this.fo.Name = "fo";
            this.fo.ReadOnly = true;
            // 
            // chi_sqr
            // 
            this.chi_sqr.HeaderText = "Chi Cua.";
            this.chi_sqr.MinimumWidth = 6;
            this.chi_sqr.Name = "chi_sqr";
            this.chi_sqr.ReadOnly = true;
            // 
            // lblDistribucionFrecuencias
            // 
            this.lblDistribucionFrecuencias.AutoSize = true;
            this.lblDistribucionFrecuencias.Location = new System.Drawing.Point(480, 60);
            this.lblDistribucionFrecuencias.Name = "lblDistribucionFrecuencias";
            this.lblDistribucionFrecuencias.Size = new System.Drawing.Size(159, 17);
            this.lblDistribucionFrecuencias.TabIndex = 24;
            this.lblDistribucionFrecuencias.Text = "Distribución frecuencias";
            // 
            // lblSumatoria
            // 
            this.lblSumatoria.AutoSize = true;
            this.lblSumatoria.Location = new System.Drawing.Point(1078, 80);
            this.lblSumatoria.Name = "lblSumatoria";
            this.lblSumatoria.Size = new System.Drawing.Size(163, 17);
            this.lblSumatoria.TabIndex = 25;
            this.lblSumatoria.Text = "Sumatoria Chi-Cuadrado";
            // 
            // txtSumatoriaChi
            // 
            this.txtSumatoriaChi.Enabled = false;
            this.txtSumatoriaChi.Location = new System.Drawing.Point(1081, 111);
            this.txtSumatoriaChi.Name = "txtSumatoriaChi";
            this.txtSumatoriaChi.Size = new System.Drawing.Size(160, 22);
            this.txtSumatoriaChi.TabIndex = 26;
            // 
            // btnSiguiente
            // 
            this.btnSiguiente.Location = new System.Drawing.Point(431, 288);
            this.btnSiguiente.Name = "btnSiguiente";
            this.btnSiguiente.Size = new System.Drawing.Size(33, 23);
            this.btnSiguiente.TabIndex = 27;
            this.btnSiguiente.Text = ">";
            this.btnSiguiente.UseVisualStyleBackColor = true;
            this.btnSiguiente.Click += new System.EventHandler(this.BtnSiguiente_Click);
            // 
            // btnAnterior
            // 
            this.btnAnterior.Location = new System.Drawing.Point(249, 288);
            this.btnAnterior.Name = "btnAnterior";
            this.btnAnterior.Size = new System.Drawing.Size(33, 23);
            this.btnAnterior.TabIndex = 27;
            this.btnAnterior.Text = "<";
            this.btnAnterior.UseVisualStyleBackColor = true;
            this.btnAnterior.Click += new System.EventHandler(this.BtnAnterior_Click);
            // 
            // lblPaginas
            // 
            this.lblPaginas.AutoSize = true;
            this.lblPaginas.Location = new System.Drawing.Point(350, 291);
            this.lblPaginas.Name = "lblPaginas";
            this.lblPaginas.Size = new System.Drawing.Size(13, 17);
            this.lblPaginas.TabIndex = 28;
            this.lblPaginas.Text = "-";
            // 
            // chtFrecuencias
            // 
            chartArea1.Name = "ChartArea1";
            this.chtFrecuencias.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chtFrecuencias.Legends.Add(legend1);
            this.chtFrecuencias.Location = new System.Drawing.Point(249, 317);
            this.chtFrecuencias.Name = "chtFrecuencias";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "FE";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "FO";
            this.chtFrecuencias.Series.Add(series1);
            this.chtFrecuencias.Series.Add(series2);
            this.chtFrecuencias.Size = new System.Drawing.Size(1001, 344);
            this.chtFrecuencias.TabIndex = 29;
            this.chtFrecuencias.Text = "chart1";
            title1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            title1.Name = "titulo";
            title1.Text = "Grafico de frecuencias";
            title2.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Left;
            title2.Name = "valores";
            title2.Text = "Valores";
            title3.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            title3.Name = "intervalos";
            title3.Text = "Intervalos";
            this.chtFrecuencias.Titles.Add(title1);
            this.chtFrecuencias.Titles.Add(title2);
            this.chtFrecuencias.Titles.Add(title3);
            // 
            // txtValorTabla
            // 
            this.txtValorTabla.Enabled = false;
            this.txtValorTabla.Location = new System.Drawing.Point(1081, 181);
            this.txtValorTabla.Name = "txtValorTabla";
            this.txtValorTabla.Size = new System.Drawing.Size(160, 22);
            this.txtValorTabla.TabIndex = 31;
            // 
            // lvlValorTabla
            // 
            this.lvlValorTabla.AutoSize = true;
            this.lvlValorTabla.Location = new System.Drawing.Point(1078, 150);
            this.lvlValorTabla.Name = "lvlValorTabla";
            this.lvlValorTabla.Size = new System.Drawing.Size(167, 17);
            this.lvlValorTabla.TabIndex = 30;
            this.lvlValorTabla.Text = "Valor tabla Chi-Cuadrado";
            // 
            // txtNivelConfianza
            // 
            this.txtNivelConfianza.Location = new System.Drawing.Point(1081, 40);
            this.txtNivelConfianza.MaxLength = 8;
            this.txtNivelConfianza.Name = "txtNivelConfianza";
            this.txtNivelConfianza.Size = new System.Drawing.Size(160, 22);
            this.txtNivelConfianza.TabIndex = 12;
            this.txtNivelConfianza.Text = "0.05";
            // 
            // lblNivelConfianza
            // 
            this.lblNivelConfianza.AutoSize = true;
            this.lblNivelConfianza.Location = new System.Drawing.Point(1078, 9);
            this.lblNivelConfianza.Name = "lblNivelConfianza";
            this.lblNivelConfianza.Size = new System.Drawing.Size(104, 17);
            this.lblNivelConfianza.TabIndex = 32;
            this.lblNivelConfianza.Text = "Nivel confianza";
            // 
            // lblAprobacion
            // 
            this.lblAprobacion.AutoSize = true;
            this.lblAprobacion.Location = new System.Drawing.Point(1123, 220);
            this.lblAprobacion.Name = "lblAprobacion";
            this.lblAprobacion.Size = new System.Drawing.Size(72, 17);
            this.lblAprobacion.TabIndex = 34;
            this.lblAprobacion.Text = "Resultado";
            // 
            // txtResultadoChi
            // 
            this.txtResultadoChi.Enabled = false;
            this.txtResultadoChi.Location = new System.Drawing.Point(1081, 240);
            this.txtResultadoChi.Name = "txtResultadoChi";
            this.txtResultadoChi.Size = new System.Drawing.Size(160, 22);
            this.txtResultadoChi.TabIndex = 31;
            // 
            // btnVerificarChi
            // 
            this.btnVerificarChi.Location = new System.Drawing.Point(1081, 268);
            this.btnVerificarChi.Name = "btnVerificarChi";
            this.btnVerificarChi.Size = new System.Drawing.Size(160, 23);
            this.btnVerificarChi.TabIndex = 13;
            this.btnVerificarChi.Text = "VERIFICAR";
            this.btnVerificarChi.UseVisualStyleBackColor = true;
            this.btnVerificarChi.Click += new System.EventHandler(this.BtnVerificar_Click);
            // 
            // Formulario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1262, 673);
            this.Controls.Add(this.lblAprobacion);
            this.Controls.Add(this.txtNivelConfianza);
            this.Controls.Add(this.lblNivelConfianza);
            this.Controls.Add(this.txtResultadoChi);
            this.Controls.Add(this.txtValorTabla);
            this.Controls.Add(this.lvlValorTabla);
            this.Controls.Add(this.chtFrecuencias);
            this.Controls.Add(this.lblPaginas);
            this.Controls.Add(this.btnAnterior);
            this.Controls.Add(this.btnSiguiente);
            this.Controls.Add(this.txtSumatoriaChi);
            this.Controls.Add(this.lblSumatoria);
            this.Controls.Add(this.lblDistribucionFrecuencias);
            this.Controls.Add(this.dgvFrecuencias);
            this.Controls.Add(this.dgvValores);
            this.Controls.Add(this.lblRangoSup);
            this.Controls.Add(this.txtRangoSup);
            this.Controls.Add(this.lblRangoInferior);
            this.Controls.Add(this.txtRangoInf);
            this.Controls.Add(this.lblGrillaValores);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.btnVerificarChi);
            this.Controls.Add(this.btnGenerar);
            this.Controls.Add(this.lblCantidadIntervalos);
            this.Controls.Add(this.txtCantidadIntervalos);
            this.Controls.Add(this.lblCantidadNumeros);
            this.Controls.Add(this.txtCantidadNumeros);
            this.Controls.Add(this.lblLambda);
            this.Controls.Add(this.txtLambda);
            this.Controls.Add(this.lblDesviacion);
            this.Controls.Add(this.txtDesviacion);
            this.Controls.Add(this.lblMedia);
            this.Controls.Add(this.txtMedia);
            this.Controls.Add(this.lblCotaSuperior);
            this.Controls.Add(this.lblCotaInferior);
            this.Controls.Add(this.txtCotaSuperior);
            this.Controls.Add(this.txtCotaInferior);
            this.Controls.Add(this.lblMetodo);
            this.Controls.Add(this.cmbMetodo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.MaximizeBox = false;
            this.Name = "Formulario";
            this.Text = "SIM Distribuciones v0.3";
            this.Load += new System.EventHandler(this.Formulario_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvValores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFrecuencias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chtFrecuencias)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbMetodo;
        private System.Windows.Forms.Label lblMetodo;
        private System.Windows.Forms.TextBox txtCotaInferior;
        private System.Windows.Forms.TextBox txtCotaSuperior;
        private System.Windows.Forms.Label lblCotaInferior;
        private System.Windows.Forms.Label lblCotaSuperior;
        private System.Windows.Forms.Label lblMedia;
        private System.Windows.Forms.TextBox txtMedia;
        private System.Windows.Forms.Label lblDesviacion;
        private System.Windows.Forms.TextBox txtDesviacion;
        private System.Windows.Forms.Label lblLambda;
        private System.Windows.Forms.TextBox txtLambda;
        private System.Windows.Forms.Label lblCantidadNumeros;
        private System.Windows.Forms.TextBox txtCantidadNumeros;
        private System.Windows.Forms.Label lblCantidadIntervalos;
        private System.Windows.Forms.TextBox txtCantidadIntervalos;
        private System.Windows.Forms.Button btnGenerar;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Label lblGrillaValores;
        private System.Windows.Forms.Label lblRangoInferior;
        private System.Windows.Forms.TextBox txtRangoInf;
        private System.Windows.Forms.Label lblRangoSup;
        private System.Windows.Forms.TextBox txtRangoSup;
        private System.Windows.Forms.DataGridView dgvValores;
        private System.Windows.Forms.DataGridViewTextBoxColumn index;
        private System.Windows.Forms.DataGridViewTextBoxColumn valor;
        private System.Windows.Forms.DataGridView dgvFrecuencias;
        private System.Windows.Forms.DataGridViewTextBoxColumn intervalo;
        private System.Windows.Forms.DataGridViewTextBoxColumn fe;
        private System.Windows.Forms.DataGridViewTextBoxColumn fo;
        private System.Windows.Forms.DataGridViewTextBoxColumn chi_sqr;
        private System.Windows.Forms.Label lblDistribucionFrecuencias;
        private System.Windows.Forms.Label lblSumatoria;
        private System.Windows.Forms.TextBox txtSumatoriaChi;
        private System.Windows.Forms.Button btnSiguiente;
        private System.Windows.Forms.Button btnAnterior;
        private System.Windows.Forms.Label lblPaginas;
        private System.Windows.Forms.DataVisualization.Charting.Chart chtFrecuencias;
        private System.Windows.Forms.TextBox txtValorTabla;
        private System.Windows.Forms.Label lvlValorTabla;
        private System.Windows.Forms.TextBox txtNivelConfianza;
        private System.Windows.Forms.Label lblNivelConfianza;
        private System.Windows.Forms.Label lblAprobacion;
        private System.Windows.Forms.TextBox txtResultadoChi;
        private System.Windows.Forms.Button btnVerificarChi;
    }
}

